import python.detector.detection_plugins.tensorflow_detection as tensorflow_detection
import python.detector.detection_plugins.yolo_detection as yolo_detection
import time


def initiate_framework(detection_param):
    if detection_param.framework == "tensorflow":
        tensorflow_detection.initiate_model(detection_param.model_path)
    elif detection_param.framework == "yolo":
        yolo_detection.initiate_model(detection_param.model_path)


def detection(detection_param, image):
    if detection_param.framework == "tensorflow":
        s = time.time()
        detection_result = tensorflow_detection.detector(detection_param.threshold, image,
                                                         detection_param.classes_list)
        e = time.time()
        print(e - s)
        return detection_result
    elif detection_param.framework == "yolo":
        s = time.time()
        detection_result = yolo_detection.detector(detection_param.threshold, detection_param.confidence,
                                                   image, detection_param.classes_list)
        e = time.time()
        print(e - s)
        return detection_result
