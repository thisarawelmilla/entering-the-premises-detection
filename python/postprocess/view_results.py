import cv2


def view(img, detection, box_list):
    for i in detection:
        for box in detection[i][0]:
            cv2.rectangle(img, (box[1], box[0]), (box[3], box[2]), (0, 0, 255), 2)
    for area in box_list:
        for box in area:
            cv2.rectangle(img, (box[1], box[0]), (box[3], box[2]), (255, 0, 0), 2)
    cv2.line(img, (150, 0), (500, 350), (255, 0, 0), 2)
    cv2.imshow("preview", img)
    cv2.waitKey(2000)
