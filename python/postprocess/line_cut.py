def check_line_cut(box, func):
    y_lower = box[3]
    cutting_point = (y_lower - func[1])/func[0]
    if cutting_point >= box[2]:
        return True
    else:
        return False


def get_count(boxes_list, coord_func_list):
    count_list = []
    all_boxes = []
    for func in coord_func_list:
        count = 0
        box_list = []
        for box in boxes_list:
            if check_line_cut(box, func):
                count += 1
                box_list.append(box)
        count_list.append(count)
        all_boxes.append(box_list)
    return count_list, all_boxes

