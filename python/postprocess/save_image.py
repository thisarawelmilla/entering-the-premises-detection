import cv2


def save(img, image_name, result_save_path):
    cv2.imwrite(result_save_path + "/" + image_name, img)
