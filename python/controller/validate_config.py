import json
import os
import logging

framework_list = ["tensorflow", "yolo"]
data_type_list = ['image', 'live_video', 'not_live_video']


class detection(object):
    def __init__(self, model_path, threshold, classes_list, framework, confidence):
        self.model_path = model_path
        self.threshold = threshold
        self.classes_list = classes_list
        self.framework = framework
        self.confidence = confidence


class queue_manager(object):
    def __init__(self, dir_path, save_path, processed_image_path, meta_data, data_type, speed):
        self.dir_path = dir_path
        self.save_path = save_path
        self.meta_data = meta_data
        self.speed = speed
        self.data_type = data_type
        self.processed_image_path = processed_image_path


def validate_queue_manager():
    try:
        dir_path = config['queue_manager']['dir_path']
        result_save_path = config['queue_manager']['result_save_path']
        processed_image_path = config['queue_manager']['processed_image_path']
        data_type = config['queue_manager']['data_type']
        if data_type != "image":
            speed = config['queue_manager']['speed']
        else:
            speed = 0
    except KeyError:
        logging.error("missing keys. dir_path, save_path, data_type, speed(if not a image) must be configured")
        return 1
    try:
        meta_data_path = config['queue_manager']['meta_data_dir']
    except KeyError:
        meta_data_path = None

    if not os.path.isdir(dir_path):
        logging.error("dir_path is not existing")
    if not os.path.exists(result_save_path):
        logging.error("result_save_path is not existing")
    if not os.path.exists(processed_image_path):
        logging.error("processed_image_path is not existing")
    if meta_data_path is not None and not os.path.exists(result_save_path):
        logging.error("meta_data_path is not existing")
    if not type(speed) == int and (0 >= speed):
        logging.error("speed must be a integer between 0 and 1")
    if data_type not in data_type:
        logging.error("invalid data type")

    detection_object = queue_manager(dir_path, result_save_path, processed_image_path, meta_data_path, data_type, speed)
    return detection_object


def validate_detector():
    try:
        model_path = config['detector']['model_path']
        threshold = config['detector']['threshold']
        classes_list = config['detector']['classes_list']
        framework = config['detector']['framework']
        if framework == "yolo":
            confidence = config['detector']['confidence']
        else:
            confidence = None
    except KeyError:
        logging.error("missing keys. model_path, threshold, image_path, meta_data, classes_list, framework, "
                      "confidence must be configured")
        return 1

    if not os.path.exists(model_path):
        logging.error("model_path is not existing")
    if not type(threshold) == int and (0 >= threshold >= 1):
        logging.error("threshold must be a integer between 0 and 1")
    if not type(classes_list) == list and (0 <= len(classes_list)):
        logging.error("threshold must be a integer between 0 and 1")
    if framework not in framework_list:
        logging.error("invalid framework")
    if framework == "yolo" and (0 >= threshold >= 1):
        logging.error("confidence must be a integer between 0 and 1")

    queue_manager_object = detection(model_path, threshold, classes_list, framework, confidence)
    return queue_manager_object


def convert_dic(config_path):
    with open(config_path) as config_file:
        global config
        config = json.load(config_file)

    detection_object = validate_detector()
    queue_manager_object = validate_queue_manager()
    return queue_manager_object, detection_object
